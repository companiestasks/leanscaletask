//
//  AppDelegate.swift
//  LeanScaleCaseStudy
//
//  Created by Hassan on 9/16/21.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        initialMainWindow()
        initialMainTabbar(withWindow: window)
        return true
    }
    
    private func initialMainWindow() -> Void {
        window = UIWindow.init(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
    }
    
    func initialMainTabbar(withWindow window: UIWindow?) -> Void {
        window?.rootViewController = MainTabBarViewController()
    }


}

