//
//  CashImagesLayer.swift
//  LeanScaleCaseStudy
//
//  Created by Hassan on 9/17/21.
//

import Foundation
import UIKit


///used as cash layer that is between component and cash liberary
struct CashImagesLayer {
    
    /// cash imageView
    /// - Parameters:
    ///   - imageView: image view that will cashed image in it
    ///   - imgUrl: image url is optional to be compatable with urls from model
    static func cashImage(imageView : UIImageView , imgUrl : String?) -> Void {
        imageView.moa.url = imgUrl
    }
}
