//
//  NavigationBarPop.swift
//  LeanScaleCaseStudy
//
//  Created by Hassan on 9/18/21.
//

import Foundation
import UIKit

protocol LargeNavigationTitle {
    func addLargeNavigationTitle()
}
extension LargeNavigationTitle where Self : UIViewController {
    func addLargeNavigationTitle() -> Void {
        self.navigationController?.navigationBar.prefersLargeTitles = true
    }
}
