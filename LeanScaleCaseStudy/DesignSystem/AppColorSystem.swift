//
//  AppColorSystem.swift
//  LeanScaleCaseStudy
//
//  Created by Hassan on 9/17/21.
//

import Foundation
import UIKit

enum AppColorSystem {
    case white
    case black
    case mainDarkGray
    case mainBlue
    case bgLightGray
    
    var color : UIColor {
        switch self {
        case .black:
            return UIColor.init(named: "black")!
        case .white:
            return UIColor.init(named: "white")!
        case .mainDarkGray:
            return UIColor.init(named: "mainDarkGray")!
        case .mainBlue:
            return UIColor.init(named: "mainBlue")!
        case .bgLightGray:
            return UIColor.init(named: "bgLightGray")!
            
        }
    }
    var layerColor : CGColor {
        return color.cgColor
    }
    
    
}

