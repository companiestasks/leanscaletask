//
//  AppFonts.swift
//  LeanScaleCaseStudy
//
//  Created by Hassan on 9/17/21.
//

import Foundation
import UIKit


private let familyName = "Helvetica"

enum AppFontSystem: String {
    case light = "Light"
    case regular = "Regular"
    case bold = "Bold"
    case extraBold = "ExtraBold"
    case semiBold = "SemiBold"
    case black = "Black"
    case medium = "Medium"
    
    
    func size(_ size: CGFloat) -> UIFont {
        if let font = UIFont(name: fullFontName, size: size ){
            return font
        }
        
//        if self == .bold {
//            return UIFont.boldSystemFont(ofSize: size)
//        }
//        return UIFont.systemFont(ofSize: size)
        fatalError("Font '\(fullFontName)' does not exist.")
    }
    fileprivate var fullFontName: String {
        return rawValue.isEmpty ? familyName : familyName + "-" + rawValue
    }
    
}
