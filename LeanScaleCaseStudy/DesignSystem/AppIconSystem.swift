//
//  AppIconSystem.swift
//  LeanScaleCaseStudy
//
//  Created by Hassan on 9/17/21.
//

import Foundation
import UIKit

struct AppIconSystem {
    static var ic_games : UIImage {
        return UIImage.init(named: "ic_games")!
    }
    static var ic_favourites : UIImage {
        return UIImage.init(named: "ic_favourites")!
    }
}
