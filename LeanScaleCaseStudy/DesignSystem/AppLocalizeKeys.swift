//
//  AppLocalizeKeys.swift
//  LeanScaleCaseStudy
//
//  Created by Hassan on 9/17/21.
//

import Foundation


struct AppLocalizeKeys {
    static var games : String {
        return "Games"
    }
    static var favourites : String {
        return "Favourites"
    }
    static var favourite : String{
        return "Favourite"
    }
}
