//
//  MainTabBarViewController.swift
//  LeanScaleCaseStudy
//
//  Created by Hassan on 9/17/21.
//

import UIKit

class MainTabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupViewControllers()
        
    }
    
    private func setupViewControllers() -> Void {
        self.viewControllers = [getGamesListVC() ,
                                getMyFavouritsVC()]
    }
    
    private func getGamesListVC() -> UIViewController {
        let gamesList = GamesListMaker.make()
        gamesList.tabBarItem = UITabBarItem.init(title: AppLocalizeKeys.games,
                                                 image: AppIconSystem.ic_games.withRenderingMode(.alwaysTemplate),
                                              tag: 0)
        return gamesList
    }
    private func getMyFavouritsVC() -> UIViewController {
        let myFavourits = MyFavouritMaker.make()
        myFavourits.tabBarItem = UITabBarItem.init(title: AppLocalizeKeys.favourites,
                                                 image: AppIconSystem.ic_favourites.withRenderingMode(.alwaysTemplate),
                                              tag: 1)
        return myFavourits
    }

}
