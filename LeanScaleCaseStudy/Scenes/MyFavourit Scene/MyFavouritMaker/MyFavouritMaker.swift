//
//  MyFavouritMaker.swift
//  LeanScaleCaseStudy
//
//  Created by Hassan on 9/17/21.
//

import Foundation
import UIKit

struct MyFavouritMaker {
    static func make() -> UIViewController {
        return UIViewController()
    }
}
